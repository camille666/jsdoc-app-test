
const fs = require('fs');
const {repositoryUrl, branch, repoName} = require('../config/index');
const { resolve } = require('path');
const { exec } = require('child_process');

const ROOT_PATH = resolve(__dirname, '../');
const MAIN_APP_PATH = resolve(__dirname, '../mainApp');
const isExist = fs.existsSync(MAIN_APP_PATH);

if (isExist) {
    console.log('如果mainApp存在，就更新')
    exec(`git fetch --all && git reset --hard ${branch} && git pull`, {
        cwd: MAIN_APP_PATH,
    })
} else {
    console.log('如果mainApp不存在，就克隆')
    exec(`git clone ${repositoryUrl} && mv ${repoName} mainApp`, {
        cwd: ROOT_PATH,
    })
}
